#include <d3d9.h>
#include <d3dx9tex.h>
class dxSprite;
class dxInput;

class gameMenu
{
public:
	bool init(dxInput* key, LPDIRECT3DDEVICE9 device);
	void update();
	int getMessage();
	void render(LPDIRECT3DDEVICE9 device);
	void resetOp();
private:
	int op;
	int selectedSet;
	dxInput* menuKey;

	dxSprite* start;
	dxSprite* startS;
	dxSprite* quit;
	dxSprite* quitS;
	dxSprite* title;
	dxSprite* background;
	dxSprite* album;
	dxSprite* albumS;
};
