#include <d3d9.h>
#include <d3dx9tex.h>

class dxMesh;
class dxInput;

class playObstacle{
public:
	bool init(LPDIRECT3DDEVICE9 device);
	void update(int updateFrames, int z);
	void render(LPDIRECT3DDEVICE9 device);
private:
	dxMesh** obstacles; // 전체 장애물 리스트
	dxMesh** items; // 전체 아이템 리스트
};