#include <d3d9.h>
#include <d3dx9tex.h>
#include <string>
using namespace std;

class dxMesh {
public:
	bool loadModel(LPDIRECT3DDEVICE9 device, string filename);
	void render(LPDIRECT3DDEVICE9 device);
	void setPosition(D3DXVECTOR3 newPosition);
	void changePosition(D3DXVECTOR3 newPosition);
	void setScale(int x, int y, int z);
	void setRotation(float x, float y, float z);
private:
	LPD3DXMESH mesh;
	D3DXVECTOR3 scale;
	D3DXVECTOR3 position;
	D3DXVECTOR3 rotation;
	DWORD materialCount;
	LPD3DXBUFFER materialBuffer;
	D3DMATERIAL9* materials;
	LPDIRECT3DTEXTURE9* textures;
};