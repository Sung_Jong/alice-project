// 12061595 �輺��
#include <d3d9.h>
#include <d3dx9tex.h>

class dxManager {
public:
	dxManager(void);
	~dxManager(void);

	bool init(HWND hwnd, int width, int height, bool fullscreen);
	void beginRender(void);
	void endRender(void);
	LPDIRECT3DDEVICE9 getD3DDevice();

private:
	LPDIRECT3D9 direct3d;
	LPDIRECT3DDEVICE9 direct3dDevice;
};