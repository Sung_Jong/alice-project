#include <d3d9.h>
#include <d3dx9tex.h>
#include "dxSprite.h"
#include "dxInput.h"
#include "gameMenu.h"

bool gameMenu::init(dxInput* key, LPDIRECT3DDEVICE9 device){
	menuKey = key;
	op = -1; // 0: quit, 1: album, 2: start
	selectedSet = 2; // 0: quit, 1: album, 2: start
	background = new dxSprite();
	background->loadSprite(device, "testBackground.png");
	background->setSize(1024, 768);
	background->setPosition(0, 0);
	title = new dxSprite();
	title->loadSprite(device, "testTitle.png");
	title->setPosition(0,0);
	title->setSize(1024, 768);
	start = new dxSprite();
	start->loadSprite(device, "testStart.png");
	start->setPosition(0,0);
	start->setSize(1024, 768);
	startS = new dxSprite();
	startS->loadSprite(device, "testStartS.png");
	startS->setPosition(0,0);
	startS->setSize(1024, 768);
	album = new dxSprite();
	album->loadSprite(device, "testStoryAlbum.png");
	album->setPosition(0,0);
	album->setSize(1024, 768);
	albumS = new dxSprite();
	albumS->loadSprite(device, "testStoryAlbumS.png");
	albumS->setPosition(0,0);
	albumS->setSize(1024, 768);
	quit = new dxSprite();
	quit->loadSprite(device, "testQuit.png");
	quit->setPosition(0,0);
	quit->setSize(1024, 768);
	quitS = new dxSprite();
	quitS->loadSprite(device, "testQuitS.png");
	quitS->setPosition(0,0);
	quitS->setSize(1024, 768);
	return true;
}

void gameMenu::update(){
	menuKey->getInput();
	if(menuKey->keyPressed(DIK_UP)){ // 0: quit, 1: album, 2: start
		selectedSet++;
	}
	if (menuKey->keyPressed(DIK_DOWN)){
		if (selectedSet == 0){
			selectedSet = 2;
		} else {
			selectedSet--;
		}
	}
	selectedSet = selectedSet%3;
	if (menuKey->keyPressed(DIK_RETURN)){
		switch(selectedSet){ // 0: quit, 1: album, 2: start
			case 2: {op = 2; break;}
			case 1: {op = 1; break;}
			case 0: {op = 0; break;}
		}
	}
}

void gameMenu::render(LPDIRECT3DDEVICE9 device){
	background->render(device, 100);
	title->render(device, 100);

	if (selectedSet == 2){
		startS->render(device, 100);
		album->render(device, 100);
		quit->render(device, 100);
	} else if (selectedSet == 1){
		start->render(device, 100);
		albumS->render(device, 100);
		quit->render(device, 100);
	} else {
		start->render(device, 100);
		album->render(device, 100);
		quitS->render(device, 100);
	}
}
void gameMenu::resetOp(){
	op = -1;
}

int gameMenu::getMessage(){
	return op;
}