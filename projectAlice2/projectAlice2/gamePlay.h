#include <d3d9.h>
#include <d3dx9tex.h>

class dxCamera;
class dxInput;
class dxTimer;

class playBackpack;
class playCharacter;
//class gameObstacles;

class gamePlay{
public:
	bool init(dxInput* key, LPDIRECT3DDEVICE9 device);
	void update();
	void render(LPDIRECT3DDEVICE9 device);

private:
	float generalZPosition;
	//general visible list �ʿ�;
	dxCamera* playCamera;
	dxInput* playKey;
	dxTimer* playTimer;
	playBackpack* backpack;
	playCharacter* character;
	//playObstacle;
};