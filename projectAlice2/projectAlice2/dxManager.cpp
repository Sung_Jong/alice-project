// 12061595 김성종
#include <d3dx9tex.h>
#include "dxManager.h"

dxManager::dxManager(void){
	direct3d = NULL;
	direct3dDevice = NULL;
}

dxManager::~dxManager(void){
	if(direct3dDevice != NULL){
		direct3dDevice->Release();
		direct3dDevice = NULL;
	}
	if(direct3d != NULL) {
		direct3d->Release();
		direct3d = NULL;
	}
}

bool dxManager::init(HWND hwnd, int width, int height, bool fullscreen){
	HRESULT hr;
	direct3d = Direct3DCreate9(D3D_SDK_VERSION);
	if (direct3d == NULL){
		return false;
	}
	// 전체화면 설정
	bool windowed = true;
	if(fullscreen){
		windowed = false;
	}
	// d3d객체 파라미터 설정
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.Windowed = windowed;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferFormat = D3DFMT_R5G6B5;
	d3dpp.BackBufferCount = 1;
	d3dpp.BackBufferHeight = height;
	d3dpp.BackBufferWidth = width;
	d3dpp.hDeviceWindow = hwnd;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;

	// 디바이스 생성
	hr = direct3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hwnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &direct3dDevice);
	if (FAILED(hr)){
		return false;
	}
	direct3dDevice->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
	return true;
}

void dxManager::beginRender(void){
	if (NULL == direct3dDevice)
		return;

	// 검은색으로 화면 초기화
	direct3dDevice->Clear(0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(120,120,120), 1.0f, 0);
	direct3dDevice->BeginScene();
}

void dxManager::endRender(void){
	direct3dDevice->EndScene();
	direct3dDevice->Present(NULL, NULL, NULL, NULL);
}

LPDIRECT3DDEVICE9 dxManager::getD3DDevice(){
	return direct3dDevice;
}