#include <d3d9.h>
#include <dinput.h>

class dxInput{
public:
	bool init(HINSTANCE hInst, HWND wndHandle);
	void getInput();
	int getMouseMovingX();
	int getMouseMovingY();
	bool isButtonDown(int button);
	bool keyDown(DWORD key);
	bool keyUp(DWORD key);
	bool keyPressed(DWORD key);
private:
	LPDIRECTINPUT dInput;
	LPDIRECTINPUTDEVICE mouseDevice;
	LPDIRECTINPUTDEVICE keyboardDevice;
	BYTE keyState[256];
	DIMOUSESTATE mouseState;
	int keyPressState[256];
};