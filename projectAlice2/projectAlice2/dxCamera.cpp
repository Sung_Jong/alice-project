#include <d3d9.h>
#include <d3dx9tex.h>
#include "dxCamera.h"

bool dxCamera::create(LPDIRECT3DDEVICE9 device, float nearView, float farView){
	myDevice = device;
	nearClip = nearView;
	farClip = farView;
	position.x = position.y = position.z = 0.0f;
	direction.x = direction.y = direction.z = 0.0f;
	up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	aspect = 1.35f;
	D3DXMatrixPerspectiveFovLH(&projectionMatrix, D3DX_PI/4.0f, aspect, nearClip, farClip);
	myDevice->SetTransform(D3DTS_PROJECTION, &projectionMatrix);
	return true;
}
void dxCamera::setPosition(int x, int y, int z){
	position.x = (float)x;
	position.y = (float)y;
	position.z = (float)z;
	resetView();
}
void dxCamera::setDirection(int x, int y, int z){
	direction.x = (float)x;
	direction.y = (float)y;
	direction.z = (float)z;
	resetView();
}
void dxCamera::resetView(void){
	D3DXMatrixLookAtLH(&viewMatrix, &position, &direction, &up);
	myDevice->SetTransform(D3DTS_VIEW, &viewMatrix);
}
