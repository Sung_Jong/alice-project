#include <d3d9.h>
#include <d3dx9tex.h>

class dxInput;
class dxMesh;
class dxSprite;

class playCharacter{
public: // 장애물 클래스가 범위에 들어가는거 추려서 보내줌
	bool init(dxInput* key, LPDIRECT3DDEVICE9 device);
	void update(int updateFrames, int z);
	int collisionCheck();// 범위 리스트, 주인공 위치, 주인공 반지름 // 배경 사이즈 -100 ~ 100
	void render(LPDIRECT3DDEVICE9 device);
private:
	dxInput* arrows;
	dxMesh* character;
	dxSprite* explosion;
	dxSprite* scored;
	float xLocation;
	float yLocation;
	float zLocation; // 그냥 받아오기만 
	float xVelocity;
	float yVelocity;
	float xAccelation;
	float yAccelation;
	int collisionResult;
};