#include <d3d9.h>
#include <d3dx9tex.h>

class dxTimer{
public:
	void init(int fps);
	int framesToUpdate(void);
private:
	LARGE_INTEGER timerFreq;
	LARGE_INTEGER timeNow;
	LARGE_INTEGER timePrev;
	int Requested_FPS;
	float intervalsPerFrame;
	float intervalsSinceLastUpdate;
};