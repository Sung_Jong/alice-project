//12061595 김성종 - www.experimentsingameprogramming.com

#include <windows.h>
#include "gameMain.h"

HINSTANCE hInst;
HWND wndHandle;

bool initWindow(HINSTANCE hInstance);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

#pragma warning( disable : 4996 )
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "dxerr.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "dxguid.lib")

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdSHow){
	// 윈도우 만들어서 등록
	if (!initWindow(hInstance)){
		MessageBox(NULL, TEXT("윈도우 생성 실패"), TEXT("ERROR"), MB_OK);
		return false;
	}
	// 여기에 게임 오브젝트 들어감
	gameMain* game = new gameMain();
	if (game->init(wndHandle, hInstance) == false){
		return 0;
	}
	// 메인 메시지 루프 시작
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	while(msg.message != WM_QUIT){
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE)){
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		} else {
			// 여기에 업데이트
			game->update();
		}
	}
	//메시지 루프가 끝나면 게임 오브젝트를 없애고 창도 종료
	if (game){
		delete game;
	}

	return (int) msg.wParam;
}

bool initWindow(HINSTANCE hInstance){
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = (WNDPROC)WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = 0;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = TEXT("Alice");
	wcex.hIconSm = 0;
	RegisterClassEx(&wcex);

	ULONG window_width = 1024;
	ULONG window_height = 768;

	DWORD style = WS_POPUP | WS_VISIBLE;
	style = WS_OVERLAPPEDWINDOW;

	wndHandle = CreateWindow(TEXT("Alice"), TEXT("Alice"), style, CW_USEDEFAULT, CW_USEDEFAULT, window_width, window_height, NULL, NULL, hInstance, NULL);

	if (!wndHandle){
		return false;
	}

	hInst = hInstance;
	ShowWindow(wndHandle, SW_SHOW);
	UpdateWindow(wndHandle);

	return true;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam){
	switch (message){
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}