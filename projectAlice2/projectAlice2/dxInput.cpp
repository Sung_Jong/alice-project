#include <d3d9.h>
#include <dinput.h> // this is a headerFile
#include "dxInput.h"

bool dxInput::init(HINSTANCE hInst, HWND wndHandle){
	HRESULT hr;
	hr = DirectInput8Create(hInst, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&dInput, NULL);

	if (FAILED(hr)){
		return false;
	}
	// mouse driver 
	
	if (FAILED(dInput->CreateDevice(GUID_SysMouse, &mouseDevice, NULL)))
		return false;
	if (FAILED(mouseDevice->SetDataFormat(&c_dfDIMouse)))
		return false;
	// 마우스 동작은 하지만 사용은 안함 - NONEXCLUSIVE
	if (FAILED(mouseDevice->SetCooperativeLevel(wndHandle, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
		return false;
	if (FAILED(mouseDevice->Acquire()))
		return false; 
	
	
	//keyboard driver
	if (FAILED(dInput->CreateDevice(GUID_SysKeyboard, &keyboardDevice, NULL)))
		return false;
	if (FAILED(keyboardDevice->SetDataFormat(&c_dfDIKeyboard)))
		return false;
	if (FAILED(keyboardDevice->SetCooperativeLevel(wndHandle, DISCL_FOREGROUND | DISCL_EXCLUSIVE)))
		return false;
	if (FAILED(keyboardDevice->Acquire()))
		return false;

	return true;
}

void dxInput::getInput(){
	HRESULT hr;
	
	hr = mouseDevice->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&mouseState);
	if (FAILED(hr)){
		mouseDevice->Acquire();
	}
	

	hr = keyboardDevice->GetDeviceState(sizeof(UCHAR[256]), (LPVOID)&keyState);
	if (FAILED(hr)){
		keyboardDevice->Acquire();
	}
}

bool dxInput::isButtonDown(int button){ //mouse button check
	if (mouseState.rgbButtons[button] & 0x80){
		return true;
	} else {
		return false;
	}
}

bool dxInput::keyDown(DWORD key){
	if (keyState[key]&0x80){
		return true;
	} else {
		return false;
	}
}
bool dxInput::keyUp(DWORD key){
	if (keyState[key]&0x80){
		return false;
	} else {
		return true;
	}
}
bool dxInput::keyPressed(DWORD key){ 
	if (keyDown(key)){
		keyPressState[key] = 1;
	}
	if (keyPressState[key] == 1){
		if (keyUp(key)){
			keyPressState[key] = 2;
		}
	}
	if (keyPressState[key] == 2){
		keyPressState[key] = 0;
		return true;
	}
	return false;
}