#include <d3d9.h>
#include <d3dx9tex.h>
#include "dxInput.h"
#include "dxMesh.h"
#include "dxSprite.h"
#include "playCharacter.h"

bool playCharacter::init(dxInput* key, LPDIRECT3DDEVICE9 device){
	character = new dxMesh();
	explosion = new dxSprite();
	scored = new dxSprite();
	arrows = key;
	xAccelation = 0.0011f; // x축은 더 30% 빨리 움직임 ㅋ
	yAccelation = 0.00083f;
	xVelocity = 0.0f;
	yVelocity = 0.0f;
	xLocation = 0.0f;
	yLocation = 0.0f;
	zLocation = 0.0f;
	collisionResult = -1;
	return true;
}

void playCharacter::update(int updateFrames, int z){ //y축 -100 ~ 100, x축 -133 ~ 133
	if (xLocation < -132.0f || xLocation > 132.0f || yLocation < -99.0f || yLocation > 99.0f){
		collisionResult = 1; // 범위 넘어가면 바로 폭발
	} else {
		for (int i = 0; i<updateFrames; i++){
			if (arrows->keyDown(DIK_LEFT)){
				xVelocity += xAccelation;
			}
			if (arrows->keyDown(DIK_RIGHT)){
				xVelocity -= xAccelation;
			}
			if (arrows->keyDown(DIK_UP)){
				yVelocity += yAccelation;
			}
				if (arrows->keyDown(DIK_DOWN)){
				yVelocity -= yAccelation;
			}
			xLocation += xVelocity;
			yLocation += yVelocity;
			zLocation = z;
			int collisionResult = collisionCheck(); // list, (x,y)coord
		}
	}
	if (!collisionResult){ // if collision happens
		return;
	}
	if (collisionResult == 1){

	}
	if (collisionResult == 2){
	}
}

void playCharacter::render(LPDIRECT3DDEVICE9 device){
	if (!collisionResult){
		character->setPosition(D3DXVECTOR3(xLocation, yLocation, zLocation));
		character->render(device);
		return;
	}
	if (collisionResult == 1){
		explosion->setPosition(xLocation,yLocation);
//		explosion->setSize();
		explosion->render(device, 100);
		return;
	}
	if (collisionResult == 2){
		scored->setPosition(xLocation,yLocation);
//		scored->setSize();
		scored->render(device, 100);
		return;
	}
}

int playCharacter::collisionCheck(){ // 충돌판정은 장애물 끝나고 ㅋ, 장애물, 아이템 선택된 리스트 받아와, sphere vs sphere
	// 0. ok, 1. with obstacle, 1. storyItem, 2. instantItem
	return 0;
}