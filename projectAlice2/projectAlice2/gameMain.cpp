#include "dxManager.h"
#include "dxInput.h"
#include "gameMain.h"
#include "gameMenu.h"
#include "gameShowMenu.h"
#include "gamePlay.h"
//temporary demo
#include "dxCamera.h"
#include "dxLight.h"
#include "dxAnimate.h"
#include "dxTimer.h"
#include "dxSprite.h"
#include "dxText.h"

bool gameMain::init(HWND wndHandle, HINSTANCE hInst){
	active = 0; // 0: menu, 1: album, 2: start
	dxMgr = new dxManager();
	dxMgr->init(wndHandle, 1024, 768, false);
	if (!dxMgr){
		return false;
	}
	keyboard = new dxInput();
	keyboard->init(hInst, wndHandle);
	
	testTimer = new dxTimer();
	testTimer->init(60);

	gamemenu = new gameMenu();
	gamemenu->init(keyboard, dxMgr->getD3DDevice());

	gameshowmenu = new gameShowMenu();
	gameshowmenu->init(keyboard, dxMgr->getD3DDevice());

	gameplay = new gamePlay();
	gameplay->init(keyboard, dxMgr->getD3DDevice());

	// for test
	testAnimate = new dxAnimate();
	testAnimate->init(dxMgr->getD3DDevice());
	testText = new dxText();
	testText->init(32, dxMgr->getD3DDevice());
	testCamera = new dxCamera();
	testCamera->create(dxMgr->getD3DDevice(), 1.0f, 700.0f);
	testCamera->setDirection(0, 0, 1);
	testCamera->setPosition(0,0,-200);
	testLights = new dxLight(dxMgr->getD3DDevice());
	int light_one = testLights->createLight();
	int light_two = testLights->createLight();
	testLights->setPosition(light_one, -100, -50, 0);
	testLights->setRange(light_one, 1000.0f);
	testLights->setPosition(light_two, 100, 50, 0);
	testLights->setRange(light_two, 1000.0f);

	return true;
}

void gameMain::update(void){
	if (active == 0){ // 0: menu, 1: album, 2: game
		gamemenu->update();
		int menuOp = gamemenu->getMessage();
		gamemenu->resetOp();
		switch (menuOp){ // inside gameMenu instructions
		case 0:{
				PostQuitMessage(0);
				break;
			   }
		case 1:{
				active = 1;
				break;
			   }
		case 2:{
				active = 2;
				break;
			   }
		}
	} else if (active == 1) { // album update
		gameshowmenu->update();
		int showmenuOp = gameshowmenu->getMessage();
		gameshowmenu->resetOp();
		switch(showmenuOp){ // 여기에 오프닝 엔딩 그리기 객체 필요
		case 6:{ // 오프닝
			break;
			   }
		case 5:{ // 엔딩1
			break;
			   }
		case 4:{ // 엔딩2
			break;
			   }
		case 3:{ // 엔딩3
			break;
			   }
		case 2:{ // 엔딩4
			break;
			   }
		case 1:{ // 만든사람 크레딧
			break;
			   }
		case 0:{
			active = 0;
			break;
			   }
		}
	} else if (active == 2) { // game update
		gameplay->update();
	}

	int framesToUpdate;
	framesToUpdate = testTimer->framesToUpdate();
	dxMgr->beginRender();

	if (active == 0){ // menu;
		gamemenu->render(dxMgr->getD3DDevice());
	} else if (active == 1) { // album;
		gameshowmenu->render(dxMgr->getD3DDevice());
	} else if (active == 2) { // game;
		gameplay->render(dxMgr->getD3DDevice());
		/*
		testText->drawText("Moving Environment test", 30, 30, 300, 300);
		testAnimate->updateFrames(framesToUpdate);
		testAnimate->render(dxMgr->getD3DDevice());
		*/
	}
	dxMgr->endRender();
}