// 12061595 �輺��

#include <d3d9.h>
#include <d3dx9tex.h>
#include <string>
using namespace std;

class dxText{
public:
	bool init(DWORD size, LPDIRECT3DDEVICE9 device);
	void drawText(string text, int x, int y, int width, int height);

private:
	ID3DXFont* g_font;
};
