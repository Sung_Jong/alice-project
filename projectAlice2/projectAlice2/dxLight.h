#include <d3d9.h>
#include <d3dx9tex.h>
#include <vector>
using namespace std;

class dxLight{
public:
	dxLight(LPDIRECT3DDEVICE9 device);
	int createLight(void);
	void enableLight(int lightNum);
	void disableLIght(int lightNum);
	void setDiffuse(int lightNum, float r, float g, float b);
	void setSpecular(int lightNum, float r, float g, float b);
	void setAmbient(int lightNum, float r, float g, float b);
	void setPosition(int lightNum, int x, int y, int z);
	void setRange(int lightNum, float newRange);
	void setDirection(int lightNum, float x, float y, float z);
private:
	LPDIRECT3DDEVICE9 myDevice;
	vector<D3DLIGHT9*> lights;
};