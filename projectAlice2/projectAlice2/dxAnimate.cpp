#include <d3d9.h>
#include <d3dx9tex.h>
#include "dxAnimate.h"

bool dxAnimate::init(LPDIRECT3DDEVICE9 device){
	scoreMesh = new dxMesh();
	scoreMesh->loadModel(device, "testSpheres.x");
	if (!scoreMesh){
		return false;
	}
	tempPosition = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	scoreMesh->setPosition(tempPosition);
	
	obstacleMesh = new dxMesh();
	obstacleMesh->loadModel(device, "testObstacles.x");
	if (!obstacleMesh){
		return false;
	}
	
	tempDir = 1;
	return true;
}
void dxAnimate::updateFrames(int numberOfFrames){
	for (int i = 0; i<numberOfFrames; i++){
		if (tempDir == 1){
			tempPosition.z += 5.0f;
		} else {
			tempPosition.z -= 5.0f;
		}
		if (tempPosition.z > 100.0f || tempPosition.z < -480.0f){
			tempDir = tempDir * -1;
		}
	}
	scoreMesh->setPosition(tempPosition);
	obstacleMesh->setPosition(tempPosition);
}
void dxAnimate::render(LPDIRECT3DDEVICE9 device){
	scoreMesh->render(device);
	obstacleMesh->render(device);
}