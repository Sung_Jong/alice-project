// 12061595 김성종

#include "dxText.h"

bool dxText::init(DWORD size, LPDIRECT3DDEVICE9 device){
	D3DXCreateFont(device, size, 0, FW_NORMAL, 1, false, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH|FF_DONTCARE, TEXT("Arial"), &g_font);
	return true;
}

void dxText::drawText(string text, int x, int y, int width, int height){
	RECT font_rect = {0,0,width,height};
	//글을 쓸 네모 설정
	SetRect(&font_rect, x, y, width, height);
	
	//글을 출력
	g_font->DrawTextA(NULL, text.c_str(), -1, &font_rect, DT_LEFT|DT_NOCLIP, 0xffffffff);
}
