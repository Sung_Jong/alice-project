#include <d3d9.h>
#include <d3dx9tex.h>
#include <string>
#include <sstream>
#include "playBackpack.h"
#include "dxSprite.h"
#include "dxInput.h"
#include "dxText.h"
using namespace std;

bool playBackpack::init(dxInput* key, LPDIRECT3DDEVICE9 device){
	boosterGauge = new dxSprite*[3];
	string tempResult;

	for (int i = 0; i<3; i++){
		stringstream tempFilename;
		tempFilename << "gauge" << (i+1) << ".png";
		tempResult = tempFilename.str();
		boosterGauge[i] = new dxSprite();
		boosterGauge[i]->loadSprite(device, tempResult);
		boosterGauge[i]->setPosition(0,0);
		boosterGauge[i]->setSize(1024,768);
	}
	storyGauge = new dxSprite*[10];
	for (int i = 0; i<10; i++){
		stringstream tempFilename;
		tempFilename << "story"<< (i+1) << ".png";
		tempResult = tempFilename.str();
		storyGauge[i] = new dxSprite();
		storyGauge[i]->loadSprite(device, tempResult);
		storyGauge[i]->setPosition(0,0);
		storyGauge[i]->setSize(1024,768);
		tempFilename.clear();
	}
	batteryInfo = new dxText();
	batteryInfo->init(32, device);
	batterySurfix = " / 100"; // total battery number indicator
	batteryScore = 0;
	batteryFinished = "0 / 100";
	spacebar = key;
	zBooster = 3;
	boosterDuration = 0; // booster Duration
	zLocation = 0.0f;
	zVelocity = 0.0f;
	zAccelation = 0.0027f;
	for (int i = 0; i <10; i++){
		storyItems[i] = 0;
	}

	debugz = new dxText();
	debugz->init(32, device);
	debugzz = "zLocation: ";
	debugzzz = "zLocation: 0";

	return true;
}
void playBackpack::update(int updateFrames){ // zLocation process
	spacebar->getInput();
	for (int i = 0; i<updateFrames; i++){
		if (spacebar->keyPressed(DIK_SPACE) && zBooster > 0){ // booster used
			zBooster--;
			zVelocity = 0.0f;
		}
		if (zVelocity < 0.5f)
			zVelocity += zAccelation;
		zLocation += zVelocity;
		
		stringstream temp;
		temp << batteryScore << batterySurfix;
		batteryFinished = temp.str(); // string concat

		stringstream temp2;
		temp2 << debugzz << zLocation;
		debugzzz = temp2.str();
	}
}
void playBackpack::render(LPDIRECT3DDEVICE9 device){ // location has to be adjusted
	batteryInfo->drawText(batteryFinished, 900, 700, 300, 300);
	debugz->drawText(debugzzz, 50,700,300,300);
	for (int i = 0; i<10; i++){
		if (storyItems[i] == 1){
			storyGauge[i]->render(device, 100);
		}
	}
	switch (zBooster){
	case 3: {boosterGauge[2]->render(device, 100); break;}
	case 2: {boosterGauge[1]->render(device, 100); break;}
	case 1: {boosterGauge[0]->render(device, 100); break;}
	case 0: {break;}
	}
}
//collision update functions by character
void playBackpack::gotStoryItem(int index){
	storyItems[index] = 1;
}

void playBackpack::gotBattery(){
	batteryScore++;
}

//booster instant items by obstacle
void playBackpack::gotBooster(){
	if (zBooster < 3)
		zBooster++;
}

// zLocation sharing
float playBackpack::getZLocation(){
	return zLocation;
}