#include <d3d9.h>
#include <d3dx9tex.h>
#include "dxSprite.h"
#include "dxInput.h"
#include "gameShowMenu.h"

bool gameShowMenu::init(dxInput* key, LPDIRECT3DDEVICE9 device){
	showMenuKey = key;
	op = -1;
	selectedSet = 6;
	showBack = new dxSprite();
	showBack->loadSprite(device, "showMenuBack.png");
	showBack->setPosition(0,0);
	showBack->setSize(1024, 768);
	opening = new dxSprite();
	opening->loadSprite(device, "showMenuOpening.png");
	opening->setPosition(0,0);
	opening->setSize(1024,768);
	openingS = new dxSprite();
	openingS->loadSprite(device, "showMenuOpeningS.png");
	openingS->setPosition(0,0);
	openingS->setSize(1024,768);
	end1 = new dxSprite();
	end1->loadSprite(device, "showMenuEnd1.png");
	end1->setPosition(0,0);
	end1->setSize(1024,768);
	end1S = new dxSprite();
	end1S->loadSprite(device, "showMenuEnd1S.png");
	end1S->setPosition(0,0);
	end1S->setSize(1024, 768);
	end2 = new dxSprite();
	end2->loadSprite(device, "showMenuEnd2.png");
	end2->setPosition(0,0);
	end2->setSize(1024,768);
	end2S = new dxSprite();
	end2S->loadSprite(device, "showMenuEnd2S.png");
	end2S->setPosition(0,0);
	end2S->setSize(1024,768);
	end3 = new dxSprite();
	end3->loadSprite(device, "showMenuEnd3.png");
	end3->setPosition(0,0);
	end3->setSize(1024, 768);
	end3S = new dxSprite();
	end3S->loadSprite(device, "showMenuEnd3S.png");
	end3S->setPosition(0,0);
	end3S->setSize(1024,768);
	end4 = new dxSprite();
	end4->loadSprite(device, "showMenuEnd4.png");
	end4->setPosition(0,0);
	end4->setSize(1024,768);
	end4S = new dxSprite();
	end4S->loadSprite(device, "showMenuEnd4S.png");
	end4S->setPosition(0,0);
	end4S->setSize(1024,768);
	credit = new dxSprite();
	credit->loadSprite(device, "showMenuCredit.png");
	credit->setPosition(0,0);
	credit->setSize(1024,768);
	creditS = new dxSprite();
	creditS->loadSprite(device, "showMenuCreditS.png");
	creditS->setPosition(0,0);
	creditS->setSize(1024,768);
	goback = new dxSprite();
	goback->loadSprite(device, "showMenuReturn.png");
	goback->setPosition(0,0);
	goback->setSize(1024,768);
	gobackS = new dxSprite();
	gobackS->loadSprite(device, "showMenuReturnS.png");
	gobackS->setPosition(0,0);
	gobackS->setSize(1024,768);
	return true;
}

void gameShowMenu::update(){
	showMenuKey->getInput();
	if(showMenuKey->keyPressed(DIK_UP)){ // 0: quit
		selectedSet++;
	}
	if (showMenuKey->keyPressed(DIK_DOWN)){
		if (selectedSet == 0){
			selectedSet = 6;
		} else {
			selectedSet--;
		}
	}
	selectedSet = selectedSet%7;
	if (showMenuKey->keyPressed(DIK_RETURN)){
		switch(selectedSet){ // 0: quit
			case 6: {op = 6; break;}
			case 5: {op = 5; break;}
			case 4: {op = 4; break;}
			case 3: {op = 3; break;}
			case 2: {op = 2; break;}
			case 1: {op = 1; break;}
			case 0: {op = 0; break;}
		}
	}
}

void gameShowMenu::render(LPDIRECT3DDEVICE9 device){
	showBack->render(device, 100);
	switch(selectedSet){
	case 6:{
		openingS->render(device, 100);
		end1->render(device,100);
		end2->render(device, 100);
		end3->render(device, 100);
		end4->render(device, 100);
		credit->render(device, 100);
		goback->render(device, 100);
		break;
		   }
	case 5:{
		opening->render(device, 100);
		end1S->render(device,100);
		end2->render(device, 100);
		end3->render(device, 100);
		end4->render(device, 100);
		credit->render(device, 100);
		goback->render(device, 100);
		break;
		   }
	case 4:{
		opening->render(device, 100);
		end1->render(device,100);
		end2S->render(device, 100);
		end3->render(device, 100);
		end4->render(device, 100);
		credit->render(device, 100);
		goback->render(device, 100);
		break;
		   }
	case 3:{
		opening->render(device, 100);
		end1->render(device,100);
		end2->render(device, 100);
		end3S->render(device, 100);
		end4->render(device, 100);
		credit->render(device, 100);
		goback->render(device, 100);
		break;
		   }
	case 2:{
		opening->render(device, 100);
		end1->render(device,100);
		end2->render(device, 100);
		end3->render(device, 100);
		end4S->render(device, 100);
		credit->render(device, 100);
		goback->render(device, 100);
		break;
		   }
	case 1:{
		opening->render(device, 100);
		end1->render(device,100);
		end2->render(device, 100);
		end3->render(device, 100);
		end4->render(device, 100);
		creditS->render(device, 100);
		goback->render(device, 100);
		break;
		   }
	case 0:{
		opening->render(device, 100);
		end1->render(device,100);
		end2->render(device, 100);
		end3->render(device, 100);
		end4->render(device, 100);
		credit->render(device, 100);
		gobackS->render(device, 100);
		break;
		   }
	}
}
int gameShowMenu::getMessage(){
	return op;
}

void gameShowMenu::resetOp(){
	op = -1;
}