#include <d3d9.h>
#include <d3dx9tex.h>

class dxCamera{
public:
	bool create(LPDIRECT3DDEVICE9 device, float nearView, float farView);
	void setPosition(int x, int y, int z);
	void setDirection(int x, int y, int z);
	void setUp(int x, int y, int z);
	void resetView(void);
private:
	float aspect;
	float nearClip;
	float farClip;
	D3DXMATRIX viewMatrix;
	D3DXMATRIX projectionMatrix;
	D3DXVECTOR3 position;
	D3DXVECTOR3 direction;
	D3DXVECTOR3 up;
	LPDIRECT3DDEVICE9 myDevice;
};