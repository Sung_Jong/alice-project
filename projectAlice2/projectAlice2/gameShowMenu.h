#include <d3d9.h>
#include <d3dx9tex.h>
class dxSprite;
class dxInput;

class gameShowMenu
{
public:
	bool init(dxInput* key, LPDIRECT3DDEVICE9 device);
	void update();
	int getMessage();
	void render(LPDIRECT3DDEVICE9 device);
	void resetOp();
private:
	int op;
	int selectedSet;
	dxInput* showMenuKey;

	dxSprite* showBack;
	dxSprite* credit;
	dxSprite* creditS;
	dxSprite* end1;
	dxSprite* end1S;
	dxSprite* end2;
	dxSprite* end2S;
	dxSprite* end3;
	dxSprite* end3S;
	dxSprite* end4;
	dxSprite* end4S;
	dxSprite* opening;
	dxSprite* openingS;
	dxSprite* goback;
	dxSprite* gobackS;
};
