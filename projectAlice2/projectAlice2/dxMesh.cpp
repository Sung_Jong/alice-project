#include "dxMesh.h"
#include <string>
using namespace std;

bool dxMesh::loadModel(LPDIRECT3DDEVICE9 device, string filename){
	HRESULT hr;
	mesh = NULL;
	scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	position = D3DXVECTOR3(0.0f,0.0f,0.0f);
	rotation = D3DXVECTOR3(0.0f,0.0f,0.0f);

	hr = D3DXLoadMeshFromX(filename.c_str(), D3DXMESH_SYSTEMMEM, device, NULL, &materialBuffer, NULL, &materialCount, &mesh);
	if (FAILED(hr)){
		return false;
	}
	
	D3DXMATERIAL* modelMaterials = (D3DXMATERIAL*)materialBuffer->GetBufferPointer();

	materials = new D3DMATERIAL9[materialCount];
	textures = new LPDIRECT3DTEXTURE9[materialCount];

	for(DWORD i=0; i<materialCount; i++){
		materials[i] = modelMaterials[i].MatD3D;
		hr = D3DXCreateTextureFromFile(device, modelMaterials[i].pTextureFilename, &textures[i]);
		if (FAILED(hr)){
			textures[i] = NULL;
		}
	}
	if (materialBuffer){
		materialBuffer->Release();
	}
	return true;
}

void dxMesh::render(LPDIRECT3DDEVICE9 device){
	D3DXMATRIX scaleMatrix; 
	D3DXMATRIX transMatrix;
	D3DXMATRIX rotationMatrix;
	//scale matrix
	D3DXMatrixScaling(&scaleMatrix, scale.x, scale.y, scale.z);
	//store the position info into translation matrix
	D3DXMatrixTranslation(&transMatrix, position.x, position.y, position.z);
	//set the rotation matrix
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, rotation.x, rotation.y, rotation.z);

	D3DXMatrixMultiply(&transMatrix, &rotationMatrix, &transMatrix);
	D3DXMatrixMultiply(&transMatrix, &scaleMatrix, &transMatrix);

	device->SetTransform(D3DTS_WORLD, &transMatrix);
	device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE); 
	device->SetRenderState(D3DRS_ZENABLE, TRUE);

	for (DWORD i = 0; i<materialCount; i++){
		device->SetMaterial(&materials[i]);
		if (textures[i] != NULL){
			device->SetTexture(0, textures[i]);
		}
		mesh->DrawSubset(i);
	}
}

void dxMesh::setPosition(D3DXVECTOR3 newPosition){
	position = newPosition;
}

void dxMesh::changePosition(D3DXVECTOR3 newPosition){
	position = newPosition;
}

void dxMesh::setScale(int x, int y, int z){
	scale.x = (float)x;
	scale.y = (float)y;
	scale.z = (float)z;
}
void dxMesh::setRotation(float x, float y, float z){
	rotation.x = x;
	rotation.y = y;
	rotation.z = z;
}