#include <d3d9.h>
#include <d3dx9tex.h>
#include <vector>
#include "dxLight.h"
using namespace std;

dxLight::dxLight(LPDIRECT3DDEVICE9 device){
	myDevice = device;
	myDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
}

int dxLight::createLight(){
	D3DLIGHT9* newLight = new D3DLIGHT9;
//	newLight->Type = D3DLIGHT_DIRECTIONAL;
//	newLight->Direction = D3DXVECTOR3(0.0f,0.0f, 1.0f);
	newLight->Type = D3DLIGHT_POINT;
	newLight->Diffuse.r = newLight->Diffuse.g = newLight->Diffuse.b = 0.5f;
	newLight->Specular.r = newLight->Specular.g = newLight->Specular.b = 0.7f;
	newLight->Ambient.r = newLight->Ambient.g = newLight->Ambient.b = 1.0f;
	newLight->Position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	newLight->Range = 100.0f;
	newLight->Attenuation0 = 1.0f;
	newLight->Attenuation1 = 0.0f;
	newLight->Attenuation2 = 0.0f;

	lights.push_back((D3DLIGHT9*) newLight);

	myDevice->SetLight((int)lights.size()-1, (D3DLIGHT9*)lights[((int)lights.size()-1)]);
	myDevice->LightEnable((int)lights.size()-1, true);
	return ((int)lights.size()-1);
}
void dxLight::enableLight(int lightNum){
	myDevice->LightEnable(lightNum, TRUE);
}
void dxLight::disableLIght(int lightNum){
	myDevice->LightEnable(lightNum, FALSE);
}
void dxLight::setDiffuse(int lightNum, float r, float g, float b){
	lights[lightNum]->Diffuse.r = r;
	lights[lightNum]->Diffuse.g = g;
	lights[lightNum]->Diffuse.b = b;
	myDevice->SetLight(lightNum, (D3DLIGHT9*)lights[lightNum]);
}
void dxLight::setSpecular(int lightNum, float r, float g, float b){
	lights[lightNum]->Specular.r = r;
	lights[lightNum]->Specular.g = g;
	lights[lightNum]->Specular.b = b;
	myDevice->SetLight(lightNum, (D3DLIGHT9*)lights[lightNum]);
}
void dxLight::setAmbient(int lightNum, float r, float g, float b){
	lights[lightNum]->Ambient.r = r;
	lights[lightNum]->Ambient.g = g;
	lights[lightNum]->Ambient.b = b;
	myDevice->SetLight(lightNum, (D3DLIGHT9*)lights[lightNum]);
}
void dxLight::setPosition(int lightNum, int x, int y, int z){
	lights[lightNum]->Position.x = (float)x;
	lights[lightNum]->Position.y = (float)y;
	lights[lightNum]->Position.z = (float)z;
	myDevice->SetLight(lightNum, (D3DLIGHT9*)lights[lightNum]);
}
void dxLight::setRange(int lightNum, float newRange){
	lights[lightNum]->Range = newRange;
	myDevice->SetLight(lightNum, (D3DLIGHT9*)lights[lightNum]);
}

void dxLight::setDirection(int lightNum, float x, float y, float z){
	lights[lightNum]->Direction.x = x;
	lights[lightNum]->Direction.y = y;
	lights[lightNum]->Direction.z = z;
	myDevice->SetLight(lightNum, (D3DLIGHT9*)lights[lightNum]);
}