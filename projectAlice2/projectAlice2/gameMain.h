// 12061595 �輺��

class dxManager;
class dxText;
class dxSprite;
class dxMesh;
class dxCamera;
class dxLight;
class dxAnimate;
class dxTimer;
class dxInput;
class gameMenu;
class gameShowMenu;
class gamePlay;

class gameMain{
public:
	bool init(HWND wndHandle, HINSTANCE hInst);
	void update(void);

private:
	int active; // 0: menu, 1: game
	gameMenu* gamemenu;
	gameShowMenu* gameshowmenu;
	dxManager* dxMgr;
	dxInput* keyboard;
	gamePlay* gameplay;

//temporary demo
	dxText* testText;
	dxSprite* testSprite;
//	dxMesh* testMesh;
	dxCamera* testCamera;
	dxLight* testLights;
	dxTimer* testTimer;
	dxAnimate* testAnimate;
};