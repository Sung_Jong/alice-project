#include <d3d9.h>
#include <d3dx9tex.h>
#include <string>
using namespace std;

class dxSprite;
class dxInput;
class dxText;

class playBackpack{
public:
	bool init(dxInput* key, LPDIRECT3DDEVICE9 device);
	void update(int updateFrames);
	void gotBattery();
	void gotStoryItem(int index);
	float getZLocation();
	void gotBooster();
	void render(LPDIRECT3DDEVICE9 device);
private:
	dxInput* spacebar;
	dxSprite** boosterGauge; // 부스터 3개 표시
	dxSprite** storyGauge; // 스토리아이템 10개 획득시 표시
	dxText* batteryInfo;
	int zBooster;
	int boosterDuration;
	float zLocation;
	float zVelocity;
	float zAccelation;
	string batteryFinished;
	string batterySurfix;
	int batteryScore;
	int storyItems[10];
	dxText* debugz;
	string debugzz;
	string debugzzz;
};