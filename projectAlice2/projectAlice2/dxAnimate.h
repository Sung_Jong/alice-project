#include <d3d9.h>
#include <d3dx9tex.h>
#include "dxMesh.h"

class dxAnimate{
public:
	bool init(LPDIRECT3DDEVICE9 device);
	void updateFrames(int numberOfFrames);
	void render(LPDIRECT3DDEVICE9 device);
private:
	int tempDir;
	D3DXVECTOR3 tempPosition;
	dxMesh* scoreMesh;
	dxMesh* obstacleMesh;
};