#include <d3d9.h>
#include <d3dx9tex.h>
#include "dxTimer.h"

void dxTimer::init(int fps){
	QueryPerformanceFrequency(&timerFreq);
	QueryPerformanceCounter(&timeNow);
	QueryPerformanceCounter(&timePrev);

	Requested_FPS = fps;

	intervalsPerFrame = ((float)timerFreq.QuadPart / Requested_FPS );
}

int dxTimer::framesToUpdate(){
	int framesToUpdate = 0;
	QueryPerformanceCounter(&timeNow);

	intervalsSinceLastUpdate = (float)timeNow.QuadPart - (float)timePrev.QuadPart;

	framesToUpdate = (int)(intervalsSinceLastUpdate / intervalsPerFrame);

	if (framesToUpdate != 0){
		QueryPerformanceCounter(&timePrev);
	}
	return framesToUpdate;
}