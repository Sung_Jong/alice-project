#include <d3d9.h>
#include <d3dx9tex.h>
#include "dxCamera.h"
#include "dxInput.h"
#include "dxTimer.h"
#include "gamePlay.h"
#include "playBackpack.h"
#include "playCharacter.h"
//#include "playObstacles.h"

bool gamePlay::init(dxInput* key, LPDIRECT3DDEVICE9 device){
	playKey = key;
	playTimer = new dxTimer();
	playTimer->init(60);

	backpack = new playBackpack();
	backpack->init(key, device);

	//generalZPosition: 배낭에서 받아온 z값
	generalZPosition = backpack->getZLocation();

	character = new playCharacter();
	character->init(key, device);

	/* obstacles = new playObstacles();
	obstacles->init(key, device); */

	//장애물 시작
	playCamera = new dxCamera();
	playCamera->create(device, 1.0f, 700.0f);
	playCamera->setDirection(0, 0, 1);
	playCamera->setPosition(0,0,-50);
	return true;
}

void gamePlay::update(){
	int framesToUpdate;
	framesToUpdate = playTimer->framesToUpdate();
	backpack->update(framesToUpdate);
	generalZPosition = backpack->getZLocation();
	//카메라 위치 갱신
	playCamera->setPosition(0,0,(generalZPosition-50)); // 이게 렌더로 들어갈수도 있음
	//playObstacle->update(updateFrames, generalZPosition);

//	character->update(framesToUpdate, generalZPosition);
}

void gamePlay::render(LPDIRECT3DDEVICE9 device){
	backpack->render(device);
	//obstacles->render(device);
//	character->render(device);
}